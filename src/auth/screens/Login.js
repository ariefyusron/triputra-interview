import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar } from 'react-native';

import FormLogin from '../components/FormLogin';

class Login extends Component {

  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor='#3498db' barStyle='light-content' />
        <FormLogin navigation={this.props.navigation} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#3498DB',
    flex: 1,
    paddingTop: '30%'
  }
})

export default Login;