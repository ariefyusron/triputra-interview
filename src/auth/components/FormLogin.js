import React, { Component } from 'react';
import { View, TextInput, TouchableOpacity, Text, StyleSheet } from 'react-native';

const FormLogin = ({navigation}) => (
  <View style={styles.container}>
    <TextInput
      placeholder='username'
      placeholderTextColor='#9A9A9A'
      style={styles.input}
    />
    <TextInput
      placeholder='password'
      placeholderTextColor='#9A9A9A'
      style={styles.input}
    />
    <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Home')}>
      <Text style={styles.buttonText}>Log in</Text>
    </TouchableOpacity>
  </View>
);

const styles = StyleSheet.create({
  container: {
    padding: 12
  },
  input: {
    backgroundColor: '#fff',
    borderRadius: 5,
    marginBottom: 12,
    paddingHorizontal: 12,
    height: 40
  },
  button: {
    height: 40,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#0172BB'
  },
  buttonText: {
    color: '#DFEEFF',
    fontWeight: 'bold'
  }
})

export default FormLogin;