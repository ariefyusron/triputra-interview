import { createStackNavigator, createAppContainer } from 'react-navigation';

import Login from '../../auth/screens/Login';
import Home from '../../home/screens/Home';
import Map from '../../map/screens/Map';

const stackNavigator = createStackNavigator({
  Login,
  Home,
  Map
},{
  defaultNavigationOptions: {
    header: null
  }
})

export default createAppContainer(stackNavigator);