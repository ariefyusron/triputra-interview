import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, StatusBar } from 'react-native';
import { DeckSwiper, Icon } from 'native-base';
import { connect } from 'react-redux';

import { skip, like } from '../action';

class Home extends Component {

  handleSwipeLeft(props){
    props.dispatch(skip(props.home.left))
  }

  handleSwipeRight(props){
    props.dispatch(like(props.home.right))
  }

  handleClickSkip(){
    this.props.dispatch(skip(this.props.home.left))
    this._deckSwiper._root.swipeLeft()
  }

  handleClickLike(){
    this.props.dispatch(like(this.props.home.right))
    this._deckSwiper._root.swipeRight()
  }

  handleNavigate(page){
    this.props.navigation.navigate(page)
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor='#0172BB' barStyle='light-content' />
        <View style={styles.header}>
          <Text style={{fontSize: 20, color: '#fff', fontWeight: 'bold'}}>Home</Text>
        </View>
        <View style={styles.content}>
          <DeckSwiper
            ref={(c) => this._deckSwiper = c}
            dataSource={this.props.home.data}
            onSwipeLeft={() => this.handleSwipeLeft(this.props)}
            onSwipeRight={() => this.handleSwipeRight(this.props)}
            renderItem={item => (
              <View style={styles.card}>
                <Image
                  source={{uri: item.image}}
                  style={styles.image}
                />
              </View>
            )}
          />
          <View style={styles.row}>
            <TouchableOpacity style={[styles.button, {backgroundColor: '#e74c3c'}]} onPress={() => this.handleClickSkip()}>
              <Icon name='dislike1' type='AntDesign' style={styles.icon} />
              <Text style={{position: 'absolute', color: '#e74c3c', bottom: '45%'}}>{this.props.home.left}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.button, {backgroundColor: '#2ecc71'}]} onPress={() => this.handleClickLike()}>
              <Icon name='like1' type='AntDesign' style={styles.icon} />
              <Text style={{position: 'absolute', color: '#2ecc71', top: '40%'}}>{this.props.home.right}</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity style={{position: 'absolute', bottom: 10, right: 15}} onPress={() => this.handleNavigate('Map')}>
            <Text>Go to map >></Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  content: {
    flex: 1,
    padding: 20
  },
  card: {
    alignSelf: 'center',
    width: 200
  },
  image: {
    width: '100%',
    height: 200,
    borderRadius: 5
  },
  row: {
    flexDirection: 'row',
    alignSelf: 'center',
    position: 'absolute',
    top: '50%',
    width: '100%',
    justifyContent: 'space-around'
  },
  icon: {
    color: '#fff',
    fontSize: 35
  },
  button: {
    width: 60,
    height: 60,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  header: {
    backgroundColor: '#3498DB',
    height: 50,
    justifyContent: 'center',
    paddingHorizontal: 20
  }
})

const mapStateToProps = (state) => ({
  home: state.home
})

export default connect(mapStateToProps)(Home);