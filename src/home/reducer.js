const initialState = {
  data: [
    {
      image: 'https://cdn.iconscout.com/icon/free/png-256/avatar-372-456324.png',
      name: 'Satu'
    },
    {
      image: 'https://cdn.iconscout.com/icon/free/png-256/avatar-369-456321.png',
      name: 'Dua'
    },
    {
      image: 'https://cdn.iconscout.com/icon/free/png-256/avatar-373-456325.png',
      name: 'Tiga'
    },
    {
      image: 'https://xvp.akamaized.net/assets/illustrations/happy-laptop-user-girl-ce42c1b1997f0128b89c6c25c55c3446.png',
      name: 'Empat'
    }
  ],
  left: 0,
  right: 0
}

const homeReducer = (state = initialState, action) => {
  switch(action.type) {
    case 'SKIP':
      return {...state, left: action.payload+1}
    case 'LIKE':
      return {...state, right: action.payload+1}
    default:
      return state;
  }
};

export default homeReducer;