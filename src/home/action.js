const skip = (data) => ({
  type: 'SKIP',
  payload: data
});

const like = (data) => ({
  type: 'LIKE',
  payload: data
});

export {
  skip,
  like
}